Для запуска приложения необходимо запустить `npm install`, а затем `npm run watch` или `npm run build`. Также необходимо установить gulp.

Работающее приложение можно увидеть по ссылке https://vibrant-khorana-6b8d5e.netlify.com/
