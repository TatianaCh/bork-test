function initLogin() {
  const data = {
    email: { value: '', isValid: false, pattern: /.+@.+\..+/i },
    password: { value: '', isValid: false, pattern: /.{5,}/ },
  };

  const inputs = document.querySelectorAll(".login__form-input input");
  const btn = document.querySelector('.login .form button');
  if (btn) {
    btn.disabled = !checkForm();
  }
  btn.addEventListener('click', onSubmitClick);
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('input', onFormInputChange);
  }

  function onSubmitClick(event) {
    event.preventDefault();
    const welcomeText = 'Welcome here, ' + data.email.value + '!';
    alert(welcomeText);
  }

  function onFormInputChange(event) {
    const elem = event.target;
    const isInputValid = elem.validity ? elem.validity.valid : false;
    data[elem.name].value = elem.value;
    data[elem.name].isValid = isInputValid && data[elem.name].pattern.test(elem.value);
    const btn = document.querySelector('.login .form button');
    if (btn) {
      btn.disabled = !checkForm();
    }
  }

  function checkForm() {
    for (let key in data) {
      if (!data[key].value || !data[key].isValid) {
        return false;
      }
    }
    return true;
  }
}

export default initLogin;