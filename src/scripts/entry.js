import initLogin from './login';

document.addEventListener("DOMContentLoaded", ready);

function ready() {
  const toggleBtnsList = document.getElementsByClassName('toggle');
  for (let i = 0; i < toggleBtnsList.length; i++) {
    toggleBtnsList[i].addEventListener('click', onToggleClick);
  }

  const popupList = document.getElementsByClassName('popup');
  for (let i = 0; i < popupList.length; i++) {
    popupList[i].addEventListener('click', onPopupClick);
  }

  const switches = document.querySelectorAll('.switch input[type="checkbox"]');
  switches.forEach((item) => {
    item.addEventListener('change', onSwitchToggle);
  })

  initLogin();
}


function onPopupClick(event) {
  const clickedContent = findClosest(event.target, '.popup__content');
  if (clickedContent) {
    const closeBtn = findClosest(event.target, '.close-btn');
    if (!closeBtn) {
      return;
    }
  }
  const element = findClosest(event.target, '.popup');
  if (element) {
    element.className = element.className.replace('active', '').trim();
  }
}

function onToggleClick(event) {
  const target = findClosest(event.target, '.toggle');
  if (!target || !target.dataset.toggle) {
    return;
  }
  const toggleElementId = target.dataset.toggle;
  const element = document.getElementById(toggleElementId);
  const elementClass = element.className.replace('active', '').trim();
  if (element.className.indexOf('active') > -1) {
    element.className = elementClass;
  } else {
    element.className = elementClass + ' active';
  }
}

function onSwitchToggle(event) {
  const element = event.target;
  const container = findClosest(element, '.switch');
  const containerClass = container.className.replace('checked', '').trim();
  if (element.checked) {
    container.className = containerClass + ' checked';
  } else {
    container.className = containerClass;
  }
}

function findClosest(element, selector) {
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector;
  }
  let node = element;
  while (node && node.matches) {
    if (node.matches(selector)) {
      return node;
    }
    node = node.parentElement;
  }
  return null;
}
